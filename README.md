# ZFS

Установка. В скрипте `script.sh`, все 1 в 1 как на слайде,
чтобы сработал modprobe пришлось перезагрузить виртуалку.

Файлы со снепшотом и экспортированным пулом
в репозиторий добавлять не стал.

## 1. Compression

У меня получилось что наилучшее сжатие обеспечивает gzip,
наверное за это он должен жрать процессор как не в себя,
zle похоже без сжатия.

<details>
<summary>console log</summary>

```
[root@zfs ~]# lsblk
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
sda      8:0    0  10G  0 disk 
└─sda1   8:1    0  10G  0 part /
sdb      8:16   0   5G  0 disk 
sdc      8:32   0   1G  0 disk 
sdd      8:48   0   1G  0 disk 
sde      8:64   0   1G  0 disk 
sdf      8:80   0   1G  0 disk 
sdg      8:96   0   1G  0 disk 

[root@zfs ~]# zpool create mypool sdc sdd
[root@zfs ~]# zfs create mypool/test
[root@zfs ~]# for fs in gzip lz4 lzjb zle; do zfs create mypool/test/${fs}; done
[root@zfs ~]# for fs in gzip lz4 lzjb zle; do zfs set compression=${fs} mypool/test/${fs}; done
[root@zfs ~]# zfs list
NAME               USED  AVAIL     REFER  MOUNTPOINT
mypool             258K  1.75G     25.5K  /mypool
mypool/test        124K  1.75G       28K  /mypool/test
mypool/test/gzip    24K  1.75G       24K  /mypool/test/gzip
mypool/test/lz4     24K  1.75G       24K  /mypool/test/lz4
mypool/test/lzjb    24K  1.75G       24K  /mypool/test/lzjb
mypool/test/zle     24K  1.75G       24K  /mypool/test/zle

[root@zfs ~]# curl -L https://raw.githubusercontent.com/mmcky/nyu-econ-370/master/notebooks/data/book-war-and-peace.txt -o "war-and-peace.txt"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 3127k  100 3127k    0     0  2407k      0  0:00:01  0:00:01 --:--:-- 2407k

[root@zfs ~]# for fs in gzip lz4 lzjb zle; do cp war-and-peace.txt /mypool/test/${fs}/; done

[root@zfs ~]# zfs list
NAME               USED  AVAIL     REFER  MOUNTPOINT
mypool            8.76M  1.74G     25.5K  /mypool
mypool/test       8.60M  1.74G       28K  /mypool/test
mypool/test/gzip  1.20M  1.74G     1.20M  /mypool/test/gzip
mypool/test/lz4   1.96M  1.74G     1.96M  /mypool/test/lz4
mypool/test/lzjb  2.33M  1.74G     2.33M  /mypool/test/lzjb
mypool/test/zle   3.08M  1.74G     3.08M  /mypool/test/zle

```
</details>


## 2. Import pool

- размер хранилища: 480M
- тип pool:         mirror-0
- recordsize:       128K
- compression:      zle
- checksum:         sha256

<details>
<summary>console log</summary>

```
[vagrant@zfs vagrant]$ tar -xf zfs_task1.tar.gz 
[root@zfs vagrant]# zpool import -d ./zpoolexport/ otus

[vagrant@zfs vagrant]$ zpool list
NAME     SIZE  ALLOC   FREE  CKPOINT  EXPANDSZ   FRAG    CAP  DEDUP    HEALTH  ALTROOT
mypool  1.88G  8.84M  1.87G        -         -     0%     0%  1.00x    ONLINE  -
otus     480M  2.09M   478M        -         -     0%     0%  1.00x    ONLINE  -

[vagrant@zfs vagrant]$ zpool status otus
  pool: otus
 state: ONLINE
  scan: none requested
config:

        NAME                            STATE     READ WRITE CKSUM
        otus                            ONLINE       0     0     0
          mirror-0                      ONLINE       0     0     0
            /vagrant/zpoolexport/filea  ONLINE       0     0     0
            /vagrant/zpoolexport/fileb  ONLINE       0     0     0

[vagrant@zfs vagrant]$ zpool get all otus
NAME  PROPERTY                       VALUE                          SOURCE
otus  size                           480M                           -
otus  capacity                       0%                             -
otus  altroot                        -                              default
otus  health                         ONLINE                         -
otus  guid                           6554193320433390805            -
otus  version                        -                              default
otus  bootfs                         -                              default
otus  delegation                     on                             default
otus  autoreplace                    off                            default
otus  cachefile                      -                              default
otus  failmode                       wait                           default
otus  listsnapshots                  off                            default
otus  autoexpand                     off                            default
otus  dedupditto                     0                              default
otus  dedupratio                     1.00x                          -
otus  free                           478M                           -
otus  allocated                      2.09M                          -
otus  readonly                       off                            -
otus  ashift                         0                              default
otus  comment                        -                              default
otus  expandsize                     -                              -
otus  freeing                        0                              -
otus  fragmentation                  0%                             -
otus  leaked                         0                              -
otus  multihost                      off                            default
otus  checkpoint                     -                              -
otus  load_guid                      17631813458465267534           -
otus  autotrim                       off                            default
otus  feature@async_destroy          enabled                        local
otus  feature@empty_bpobj            active                         local
otus  feature@lz4_compress           active                         local
otus  feature@multi_vdev_crash_dump  enabled                        local
otus  feature@spacemap_histogram     active                         local
otus  feature@enabled_txg            active                         local
otus  feature@hole_birth             active                         local
otus  feature@extensible_dataset     active                         local
otus  feature@embedded_data          active                         local
otus  feature@bookmarks              enabled                        local
otus  feature@filesystem_limits      enabled                        local
otus  feature@large_blocks           enabled                        local
otus  feature@large_dnode            enabled                        local
otus  feature@sha512                 enabled                        local
otus  feature@skein                  enabled                        local
otus  feature@edonr                  enabled                        local
otus  feature@userobj_accounting     active                         local
otus  feature@encryption             enabled                        local
otus  feature@project_quota          active                         local
otus  feature@device_removal         enabled                        local
otus  feature@obsolete_counts        enabled                        local
otus  feature@zpool_checkpoint       enabled                        local
otus  feature@spacemap_v2            active                         local
otus  feature@allocation_classes     enabled                        local
otus  feature@resilver_defer         enabled                        local
otus  feature@bookmark_v2            enabled                        local

[vagrant@zfs vagrant]$ zfs get all otus/hometask2
NAME            PROPERTY              VALUE                  SOURCE
otus/hometask2  type                  filesystem             -
otus/hometask2  creation              Fri May 15  4:18 2020  -
otus/hometask2  used                  1.88M                  -
otus/hometask2  available             350M                   -
otus/hometask2  referenced            1.88M                  -
otus/hometask2  compressratio         1.00x                  -
otus/hometask2  mounted               yes                    -
otus/hometask2  quota                 none                   default
otus/hometask2  reservation           none                   default
otus/hometask2  recordsize            128K                   inherited from otus
otus/hometask2  mountpoint            /otus/hometask2        default
otus/hometask2  sharenfs              off                    default
otus/hometask2  checksum              sha256                 inherited from otus
otus/hometask2  compression           zle                    inherited from otus
otus/hometask2  atime                 on                     default
otus/hometask2  devices               on                     default
otus/hometask2  exec                  on                     default
otus/hometask2  setuid                on                     default
otus/hometask2  readonly              off                    default
otus/hometask2  zoned                 off                    default
otus/hometask2  snapdir               hidden                 default
otus/hometask2  aclinherit            restricted             default
otus/hometask2  createtxg             216                    -
otus/hometask2  canmount              on                     default
otus/hometask2  xattr                 on                     default
otus/hometask2  copies                1                      default
otus/hometask2  version               5                      -
otus/hometask2  utf8only              off                    -
otus/hometask2  normalization         none                   -
otus/hometask2  casesensitivity       sensitive              -
otus/hometask2  vscan                 off                    default
otus/hometask2  nbmand                off                    default
otus/hometask2  sharesmb              off                    default
otus/hometask2  refquota              none                   default
otus/hometask2  refreservation        none                   default
otus/hometask2  guid                  3809416093691379248    -
otus/hometask2  primarycache          all                    default
otus/hometask2  secondarycache        all                    default
otus/hometask2  usedbysnapshots       0B                     -
otus/hometask2  usedbydataset         1.88M                  -
otus/hometask2  usedbychildren        0B                     -
otus/hometask2  usedbyrefreservation  0B                     -
otus/hometask2  logbias               latency                default
otus/hometask2  objsetid              81                     -
otus/hometask2  dedup                 off                    default
otus/hometask2  mlslabel              none                   default
otus/hometask2  sync                  standard               default
otus/hometask2  dnodesize             legacy                 default
otus/hometask2  refcompressratio      1.00x                  -
otus/hometask2  written               1.88M                  -
otus/hometask2  logicalused           963K                   -
otus/hometask2  logicalreferenced     963K                   -
otus/hometask2  volmode               default                default
otus/hometask2  filesystem_limit      none                   default
otus/hometask2  snapshot_limit        none                   default
otus/hometask2  filesystem_count      none                   default
otus/hometask2  snapshot_count        none                   default
otus/hometask2  snapdev               hidden                 default
otus/hometask2  acltype               off                    default
otus/hometask2  context               none                   default
otus/hometask2  fscontext             none                   default
otus/hometask2  defcontext            none                   default
otus/hometask2  rootcontext           none                   default
otus/hometask2  relatime              off                    default
otus/hometask2  redundant_metadata    all                    default
otus/hometask2  overlay               off                    default
otus/hometask2  encryption            off                    default
otus/hometask2  keylocation           none                   default
otus/hometask2  keyformat             none                   default
otus/hometask2  pbkdf2iters           0                      default
otus/hometask2  special_small_blocks  0                      default
```
</details>

## 3. Apply snapshot

Secret message: https://github.com/sindresorhus/awesome

<details>
<summary>console log</summary>

```
[root@zfs ~]# zfs receive otus/storage < otus_task2.file 
[root@zfs ~]# find /otus/storage -type f -name secret_message | xargs cat
https://github.com/sindresorhus/awesome
```
</details>
